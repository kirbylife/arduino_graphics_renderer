#include <VGAX.h>
VGAX vga;

char mystr[5];

void setup() {
  //pinMode(LED_BUILTIN, OUTPUT);
  vga.begin();
  Serial.begin(9600);
  vga.clear(1);
}

void loop() {
  boolean sync = getIns() == '|';
  if(!sync)
    return;

  switch(getIns()) {
    case 0: {
      Serial.readBytes(mystr,1);
      // byte color = getIns();
      byte color = mystr[0];
      vga.clear(color);
      break;
    };
    case 1: {
      Serial.readBytes(mystr,3);
      /*byte x = getIns();
      byte y = getIns();
      byte color = getIns();*/
      byte x = mystr[0];
      byte y = mystr[1];
      byte color = mystr[2];
      vga.putpixel(x, y, color);
    };
    case 2: {
      Serial.readBytes(mystr,5);
      /*byte x = getIns();
      byte y = getIns();
      byte width = getIns();
      byte height = getIns();
      byte color = getIns();*/
      byte x = mystr[0];
      byte y = mystr[1];
      byte width = mystr[2];
      byte height = mystr[3];
      byte color = mystr[4];
      vga.fillrect(x, y, width, height, color);
    }
  }
}

byte getIns() {
  while(Serial.available() < 0) {
  }
  byte output;
  do {
    output = Serial.read();
  } while(output == 255);
  return output;
}
